function semGrade(a) {
  let result;
  if (a >= 70) {
    result = 'Your grade is ' + a + '%. You passed the semester. Congratulations!';
  } else {
    result = 'Your grade is ' + a + '%. You failed the semester, please don\'t give up!';
  }
  return result;
}

console.log(semGrade(70));